#!/bin/sh

# wrapper around console to enable use of a config-file.
# /jh

file=/etc/conserver/console.conf
if [ -f $file ]; then
	. $file
else
	echo "console not configured, using default 'console' as conserver"
fi

while [ $# -gt 0 ]; do
	case "$1" in
	-p)
		PORT=$2
		shift 2
		;;
	-M)
		CONSERVER=$2
		shift 2
		;;
	*)
		arg="$arg $1"
		shift
		;;
	esac
done

if [ "$CONSERVER" ]; then
	set -- -p $PORT -M $CONSERVER $arg
else
	set -- $arg
fi
exec /usr/lib/conserver-client/console "$@"
